<?php
/**
 * Template part for displaying location sections.
 *
 * @package Fiera
 */

global $block;

$map_output = '';
if ( ! empty( $block['map'] ) ) {
	//$map_output = '<div class="map" data-marker="' . esc_attr( json_encode( $block['map'] ) ) . '"></div><!-- /.map -->';
    $map_output = apply_filters( 'the_content', $block['map'] );
}
?>
<div<?php the_block_class(); ?><?php the_block_id(); ?><?php the_block_attrs(); ?>>
	<?php
	if ( 'left' === $block['map_position'] ) {
		echo '<div class="map">'.$map_output.'</div>'; // WPCS: XSS OK.
	} ?>
    <?php
	if ( 'full' === $block['map_position'] ) {
		echo $map_output; // WPCS: XSS OK.
	} else { ?>
	<div class="custom-block__content custom-block__content--fluid">
		<?php echo apply_filters( 'the_content', $block['content'] ); // WPCS: XSS OK. ?>
	</div><!-- /.map content -->
    <?php } ?>
	<?php
	if ( 'right' === $block['map_position'] ) {
		echo '<div class="map">'.$map_output.'</div>'; // WPCS: XSS OK.
	} ?>
	<?php the_block_overlay(); ?>
</div><!-- /.hero block -->
